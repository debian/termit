# Termit translation file.
# Copyright (C) 2007
# This file is distributed under the same license as the termit package.
# Evgeny Ratnikov ratnikov.ev@gmail.com 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2011-12-19 21:23+0100\n"
"Last-Translator: Dennis Ploeger <develop@dieploegers.de>\n"
"Language-Team: de\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/termit_preferences.c:218
msgid "Apply to all tabs"
msgstr ""

#: src/termit_preferences.c:206
msgid "Background"
msgstr "Hintergrund"

#: src/termit_core_api.c:440
msgid "Cannot create a new tab"
msgstr "Konnte keinen neuen Tab erzeugen"

#: src/termit_core_api.c:392
msgid "Cannot parse command. Creating tab with shell"
msgstr ""
"Konnte Befehl nicht untersuchen. Erstelle einen Tab mit der Kommandozeile"

#: src/termit.c:187 src/termit.c:226
msgid "Copy"
msgstr ""

#: src/termit.c:170 src/termit.c:223
msgid "Delete"
msgstr ""

#: src/termit.c:190
msgid "Edit"
msgstr "Bearbeiten"

#: src/termit.c:173
msgid "File"
msgstr "Datei"

#: src/termit_preferences.c:192
msgid "Font"
msgstr "Schriftart"

#: src/termit_preferences.c:199
msgid "Foreground"
msgstr "Vordergrund"

#. File menu
#: src/termit.c:169 src/termit.c:222
msgid "Open"
msgstr ""

#. Sessions menu
#: src/termit.c:202 src/callbacks.c:395
msgid "Open session"
msgstr "Sitzung öffnen"

#: src/termit.c:188 src/termit.c:227
msgid "Paste"
msgstr ""

#: src/termit.c:186 src/termit.c:225 src/termit_preferences.c:166
msgid "Preferences"
msgstr ""

#: src/termit.c:171 src/termit.c:228
msgid "Quit"
msgstr ""

#: src/termit.c:203 src/callbacks.c:365
msgid "Save session"
msgstr "Sitzung speichern"

#: src/termit.c:229
msgid "Scrollbar"
msgstr "Bildlaufleiste"

#: src/termit.c:205
msgid "Sessions"
msgstr "Sitzungen"

#. Edit menu
#: src/termit.c:185 src/termit.c:224
msgid "Set tab name..."
msgstr "Tab-Namen setzen..."

#: src/callbacks.c:41
msgid ""
"Several tabs are opened.\n"
"Close anyway?"
msgstr ""
"Mehrere Tabs sind geöffnet.\n"
"Trotzdem schließen?"

#: src/callbacks.c:274 src/callbacks.c:285
msgid "Tab name"
msgstr "Tab-Name"

#: src/termit_preferences.c:186
msgid "Title"
msgstr "Titel"

#: src/sessions.c:90
#, c-format
msgid "Unable to create directory '%s': %s"
msgstr "Konnte Verzeichnis '%s' nicht erstellen: %s"

#: src/termit_preferences.c:212
#, fuzzy
msgid "audible bell"
msgstr "Glocke"

#~ msgid "Background image"
#~ msgstr "Hintergrundbild"

#~ msgid "Image"
#~ msgstr "Bild"

#~ msgid "Transparency"
#~ msgstr "Transparenz"

#~ msgid "Visible bell"
#~ msgstr "Sichtbare Glocke"

#~ msgid "images"
#~ msgstr "Bilder"
